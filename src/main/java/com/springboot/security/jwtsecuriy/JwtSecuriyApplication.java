package com.springboot.security.jwtsecuriy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtSecuriyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSecuriyApplication.class, args);
	}

}
